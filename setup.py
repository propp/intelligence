from setuptools import setup
from intelligence import __version__

setup(
    name='intelligence',
    version='.'.join(map(str, __version__)),
    packages=['intelligence', ],
    url='https://bitbucket.org/propp/intelligence',
    author='Dmitry Sobolev',
    author_email='dmitry.n.sobolev@gmail.com',
    description='',
    install_requires=[
        'rethinkdb>=2.3,<2.4',
        'python-levenshtein>=0.12,<0.13',
        'googlemaps>=2.4,<2.5'
    ],
    entry_points={
        'console_scripts': ['intelligence=intelligence.command_line:main']
    }
)
