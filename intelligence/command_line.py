from argparse import ArgumentParser
from googlemaps.client import Client
from rethinkdb import DEFAULT_PORT

from intelligence.api import Api
from intelligence.shell import run_cli
from intelligence.db import DbConnection

__all__ = ['main', ]


def main():
    parser = ArgumentParser(description='Intelligence service agents management app')
    parser.add_argument('--host', dest='db_host', default='localhost',
                        help='Hostname or ip-address RethinkDB database')
    parser.add_argument('--port', dest='db_port', default=DEFAULT_PORT,
                        help='Port number to use in connection to RethinkDB')
    parser.add_argument('--key', dest='api_key', help='Google Maps Services API key', required=True)
    args = parser.parse_args()

    db = DbConnection('agents', host=args.db_host, port=args.db_port)
    gm_client = Client(args.api_key)
    api = Api(db, gm_client)

    run_cli(api)


if __name__ == '__main__':
    main()
