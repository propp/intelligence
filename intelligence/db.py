import rethinkdb

__all__ = ['DbConnection', 'DbNotFoundError', ]

AGENTS_TABLE = 'agents'
INDEX_LOCATION = 'location'
LEVENSHTEIN_DISTANCE = 3


class DbError(BaseException):
    pass


class DbNotFoundError(DbError):
    pass


class DbConnection:
    """
    Class DbConnection is interface for accessing and writing data in RethinkDB
    """
    def __init__(self, db_name, client=rethinkdb, host='localhost', port=rethinkdb.DEFAULT_PORT):
        super(DbConnection, self).__init__()
        self._client = client
        self._conn = self._client.connect(host=host, port=port)
        self._configure_connection(db_name)

    def _configure_connection(self, db_name):
        db_list = self._client.db_list().run(self._conn)
        if db_name not in db_list:
            self._client.db_create(db_name).run(self._conn)

        self._conn.use(db_name)

        table_list = self._client.table_list().run(self._conn)
        if AGENTS_TABLE not in table_list:
            self._client.table_create(AGENTS_TABLE).run(self._conn)
            self._client.table(AGENTS_TABLE).wait().run(self._conn)

        indices = self._client.table(AGENTS_TABLE).index_list().run(self._conn)
        if INDEX_LOCATION not in indices:
            self._client.table(AGENTS_TABLE).index_create(INDEX_LOCATION, geo=True).run(self._conn)
            self._client.table(AGENTS_TABLE).index_wait(INDEX_LOCATION).run(self._conn)

    def get_by_name(self, name):
        """
        Method trying to get agent data by name.
        If agent found method return dictionary of format {'name': <agent's name>, 'city': <agent's city>}.
        Otherwise it returns None.

        :param str name:
        :return dict|None:
        """
        agent = self._get_by_name(name)
        return self._prepare_agent_object_to_return(agent)

    def add_agent(self, name, city, location):
        """
        Method writes agents data into DB. If agent with same name name already exists, it updates record.

        :param str name:
        :param str city:
        :param tuple location:
        :return:
        """
        self._client.table(AGENTS_TABLE).insert([{
            'id': name,
            'city': city,
            'location': self._client.point(*location)
        }], conflict='update').run(self._conn)

    def get_nearest(self, name):
        """
        Method looks for nearest agents to given agent. It returns all found agents and theirs distance to given agent
        or raising exception if given agent not found by name or there are not other agents besides given.

        :param string name:
        :return list, float:
        """
        agent = self._get_by_name(name)
        if agent is None:
            raise DbNotFoundError('\'{}\' not found'.format(name))

        place = self._client.table(AGENTS_TABLE) \
            .map(lambda a: a.merge({'distance': a['location'].distance(agent['location'], unit='km')})) \
            .filter(self._client.row['id'] != agent['id']) \
            .min('distance') \
            .pluck('distance', 'location') \
            .run(self._conn)
        cursor = self._client.table(AGENTS_TABLE) \
            .get_intersecting(place['location'], index=INDEX_LOCATION) \
            .filter(self._client.row['id'] != agent['id']) \
            .run(self._conn)

        agents = [self._prepare_agent_object_to_return(a) for a in cursor]
        if len(agents) == 0:
            raise DbError('Can not found nearest agent')

        return agents, place['distance']

    def get_similar_names(self, name):
        """
        Method looks for agents which names are similar to given name (has Levenshtein's distanse <= 3)

        :param str name:
        :return iterable:
        """
        import Levenshtein

        cursor = self._client.table(AGENTS_TABLE).pluck('id').run(self._conn)
        results = filter(lambda a: Levenshtein.distance(a['id'], name) <= LEVENSHTEIN_DISTANCE, cursor)
        cursor.close()
        return sorted([r['id'] for r in results])

    def load(self, data):
        """
        Method load batch of data into DB. It returns number of inserted records and number of errors

        :param iterable data:
        :return int, int:
        """
        data_to_insert = ({
                              'id': r['name'],
                              'city': r['city'],
                              'location': self._client.point(*r['location']),
                          } for r in data)
        result = self._client.table(AGENTS_TABLE) \
            .insert(data_to_insert, conflict='update') \
            .run(self._conn)

        return result['inserted'], result['errors']

    def dump(self):
        """
        Method dumps all data to list and return it.

        :return list:
        """
        cursor = self._client.table(AGENTS_TABLE) \
            .map(lambda a: a.merge({'name': a['id']})) \
            .pluck('name', 'city').run(self._conn)

        results = list(cursor)
        cursor.close()
        return results

    def _get_by_name(self, name):
        return self._client.table(AGENTS_TABLE).get(name).run(self._conn)

    def _prepare_agent_object_to_return(self, agent):
        if agent is None:
            return None

        return {
            'name': agent['id'],
            'city': agent['city']
        }
