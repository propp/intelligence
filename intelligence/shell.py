import readline
import sys
from intelligence import __version__
from intelligence.commands import CommandManager, CommandNotFound

__all__ = ['ConsoleError', 'ConsoleSyntaxError', 'Console', 'IntelligenceConsole', 'ConsoleCompleter', 'run_cli']


class ConsoleError(BaseException):
    pass


class ConsoleSyntaxError(ConsoleError):
    pass


class Console:
    """
    Base console
    """
    prompt = '> '

    def __init__(self, prompt=None, file=sys.stdout):
        super(Console, self).__init__()
        if prompt is not None:
            self.prompt = prompt
        if file is not None:
            if not hasattr(file, 'write'):
                raise ConsoleError('\'file\' argument must have method write')
            self.out = file

    def run_loop(self, banner=None):
        if banner is None:
            banner = self.default_banner()

        self.write(banner)

        while True:
            try:
                try:
                    line = self.raw_input(self.prompt)
                except EOFError:
                    self.write('\n')
                    break
                else:
                    self.run(line)
            except KeyboardInterrupt:
                self.write('\nInterrupted by keyboard')
                break

    def default_banner(self):
        from textwrap import dedent
        return dedent('''\
        Command line interface for intelligence service agents manager.
        Version {0}.'''.format('.'.join(map(str, __version__))))

    def raw_input(self, prompt):
        """
        Write prompt and read line

        :param str prompt:
        :return str:
        """
        return input(prompt)

    def run(self, line):
        raise NotImplementedError

    def write(self, line):
        self.out.write('{}\n'.format(line))


class IntelligenceConsole(Console):
    """
    Console class of intelligence app

    """
    def __init__(self, cmd_manager, prompt=None, file=sys.stdout):
        super(IntelligenceConsole, self).__init__(prompt, file)

        if not isinstance(cmd_manager, CommandManager):
            raise ConsoleError('Argument \'cmd_manager\' must object of CommandManager class')
        self.cmd_manager = cmd_manager

    def parse_line(self, line):
        """
        Method for parsing line entered into console. It return command and it's arguments or None if nothing entered.

        :param str|None line:
        :return tuple|None:
        """
        if line is None:
            return None

        line = line.strip()
        if line == '':
            return None

        name, *args = line.split(' ')
        args = list(filter(lambda l: l != '', args))

        if len(args) == 0:
            raise ConsoleSyntaxError('Command must have at least one argument')

        return name, tuple(args)

    def run(self, line):
        """
        Method runs command that has been entered into console and write output

        :param str line:
        :return:
        """
        try:
            parsing_result = self.parse_line(line)
        except ConsoleSyntaxError as e:
            self.write('Command syntax error: {}'.format(str(e)))
            return

        if parsing_result is None:
            self.write('')
            return

        cmd_name, args = parsing_result
        try:
            cmd_func = self.cmd_manager.get_function(cmd_name)
        except CommandNotFound:
            self.write('Command \'{}\' not found'.format(cmd_name))
            return

        result = cmd_func(*args)
        self.write(result)


class ConsoleCompleter:
    """
    Simple console completer for readline lib
    """
    commands = []

    def __init__(self, options):
        self.commands = sorted(options)
        self.matches = []

    def complete(self, text, state):
        if state == 0:
            if text:
                self.matches = [s for s in self.commands if s.lower().startswith(text.lower())]
            else:
                self.matches = list(self.commands)

        try:
            response = self.matches[state]
        except IndexError:
            response = None

        return response


def run_cli(api, banner=None):
    """

    :param .api.Api api:
    :param banner:
    :return:
    """
    cmd_manager = CommandManager([
        ('add', api.add),
        ('where', api.where),
        ('help', api.help),
        ('load', api.load),
        ('save', api.save),
    ])

    readline.parse_and_bind('tab: complete')
    readline.set_completer(ConsoleCompleter(cmd_manager.get_commands()).complete)

    console = IntelligenceConsole(cmd_manager)
    console.run_loop(banner)
