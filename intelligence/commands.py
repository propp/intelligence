__all__ = ['CommandError', 'CommandNotFound', 'CommandManager']


class CommandError(BaseException):
    pass


class CommandNotFound(CommandError):
    pass


class CommandManager:
    """
    Simple class for registering commands and theirs handlers
    """
    def __init__(self, commands=()):
        self.commands = {}
        if not isinstance(commands, (list, tuple, )):
            raise CommandError('Invalid value of \'commands\' argument. Must be either list or tuple.')

        for (cmd_name, func) in commands:
            self.register_command(cmd_name, func)

    def register_command(self, name, func):
        """
        Register command and it's handler

        :param str name:
        :param callable func:
        :return:
        """
        if not callable(func):
            raise CommandError('Argument \'func\' must be callable for name \'{0}\''.format(name))

        self.commands[name] = func

    def get_commands(self):
        """
        Get list of registered commands

        :return list:
        """
        return list(self.commands.keys())

    def get_function(self, name):
        """
        Get registered command's handler

        :param name:
        :return:
        """
        try:
            return self.commands[name]
        except KeyError:
            raise CommandNotFound('Unknown command name \'{}\''.format(name))
