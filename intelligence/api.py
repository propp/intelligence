from intelligence.db import DbError, DbNotFoundError
from googlemaps.geocoding import geocode

__all__ = ['Api', 'ApiResponse', 'ApiErrorResponse', 'ApiErrorResponseWithSuggestions', 'ApiCityResponse',
           'ApiHelperResponse']


class ApiError(BaseException):
    pass


class ApiCityNotFound(ApiError):
    pass


class ApiResponse:
    """
    Base class for responses of API methods
    """
    @property
    def is_successful(self):
        return True

    def __str__(self):
        return "OK"


class ApiErrorResponse(ApiResponse):
    """
    Base class for error responses of API methods
    """
    def __init__(self, error_msg):
        super(ApiErrorResponse, self).__init__()
        self.error_msg = error_msg

    @property
    def is_successful(self):
        return False

    def __str__(self):
        return self.error_msg


class ApiErrorResponseWithSuggestions(ApiErrorResponse):
    def __init__(self, error_msg, suggestions):
        super(ApiErrorResponseWithSuggestions, self).__init__(error_msg)
        self.suggestions = suggestions

    def __str__(self):
        msg = self.error_msg
        if len(self.suggestions) > 0:
            msg = '{}\nSuggestions: {}'.format(msg, ', '.join(map(lambda s: '\'{}\''.format(s), self.suggestions)))
        return msg


class ApiCityResponse(ApiResponse):
    def __init__(self, city):
        super(ApiCityResponse, self).__init__()
        self.city = city

    def __str__(self):
        return self.city


class ApiHelperResponse(ApiResponse):
    def __init__(self, agents_data, distance):
        super(ApiHelperResponse, self).__init__()
        self.names = [agent.get('name') for agent in agents_data]
        self.distance = distance

    def __str__(self):
        return '\n'.join((self._format_name(name) for name in self.names))

    def _format_name(self, name):
        return '{0} ({1:.1f} km)'.format(name, self.distance)


class ApiLoadResponse(ApiResponse):
    def __init__(self, total, validation_errors, inserted, insert_errors):
        super(ApiLoadResponse, self).__init__()
        self.total = total
        self.inserted = inserted
        self.insert_errors = insert_errors
        self.validation_errors = validation_errors

    def __str__(self):
        msg = 'OK\nTotal: {}, Inserted: {}, Validation errors: {}, Insertion errors: {}'.format(
            self.total, self.inserted, len(self.validation_errors), self.insert_errors
        )

        if len(self.validation_errors) > 0:
            msg += '\nValidation errors:\n{}'.format('\n'.join(
                map(lambda e: 'Line {}: {}'.format(e['line'], e['cause']), self.validation_errors)))

        return msg


class Api:
    """
    Class Api contains methods that implement app functionality
    """

    def __init__(self, db_conn, gm_client):
        """

        :param intelligence.db.DbConnection db_conn:
        :param googlemaps.client.Client gm_client:
        """
        super(Api, self).__init__()
        self._db_conn = db_conn
        self._gm_client = gm_client

    def add(self, name, city):
        """
        Adding or moving agent

        :param str name:
        :param str city:
        :return ApiResponse:
        """
        try:
            city, location = self._find_city(city)
        except ApiError as e:
            return ApiErrorResponse(str(e))

        self._db_conn.add_agent(name, city, location)
        return ApiResponse()

    def where(self, name):
        """
        Find agent's location.

        :param str name:
        :return ApiResponse:
        """
        agent = self._db_conn.get_by_name(name)
        if agent is None:
            suggestions = self._db_conn.get_similar_names(name)
            return ApiErrorResponseWithSuggestions('\'{}\' not found'.format(name), suggestions)

        return ApiCityResponse(city=agent['city'])

    def help(self, name):
        """
        Find nearest to agent helper.

        :param str name:
        :return ApiResponse:
        """
        try:
            helper, distance = self._db_conn.get_nearest(name)
        except DbNotFoundError:
            suggestions = self._db_conn.get_similar_names(name)
            return ApiErrorResponseWithSuggestions('\'{}\' not found'.format(name), suggestions)
        except DbError as e:
            return ApiErrorResponse(str(e))

        return ApiHelperResponse(helper, distance)

    def load(self, file_path):
        """
        Load agents data from given CSV file. File must have following format:
        * there is two columns: name and city
        * first line must contain header
        * separator is `,`
        * quote is `"`

        :param str file_path:
        :return ApiResponse:
        """
        from csv import DictReader
        try:
            f = open(file_path)
        except FileNotFoundError:
            return ApiErrorResponse('File \'{}\' not found'.format(file_path))
        reader = DictReader(f, dialect='unix')

        total, cleaned, errors = self._clean_data(reader)
        inserted, insert_errors = self._db_conn.load(cleaned)
        f.close()

        return ApiLoadResponse(total, errors, inserted, insert_errors)

    def save(self, file_path):
        """
        Method saves agents data into CSV-file that placed in given path

        :param str file_path:
        :return ApiResponse:
        """
        from csv import DictWriter
        try:
            f = open(file_path, mode='w')
        except FileNotFoundError:
            return ApiErrorResponse('File \'{}\' not found'.format(file_path))
        writer = DictWriter(f, fieldnames=['name', 'city'], dialect='unix')

        writer.writeheader()
        writer.writerows(self._db_conn.dump())

        f.close()
        return ApiResponse()

    def _clean_data(self, data):
        def error_data(line, cause):
            return {
                'line': line,
                'cause': cause,
            }

        cleaned = []
        errors = []

        for record in data:
            try:
                name = record['name']
                city = record['city']
            except KeyError as e:
                errors.append(error_data(data.line_num, 'Field \'{}\' not found'.format(e.args[0])))
                continue

            try:
                city, location = self._find_city(city)
            except ApiCityNotFound as e:
                errors.append(error_data(data.line_num, str(e)))
                continue

            cleaned.append({
                'name': name,
                'city': city,
                'location': location
            })

        total = data.line_num - 1
        return total, cleaned, errors

    def _find_city(self, city):
        results = geocode(self._gm_client, city)

        if len(results) == 0:
            raise ApiCityNotFound('\'{}\' not found'.format(city))

        first_result = results[0]
        location = first_result['geometry']['location']
        return city, (location['lng'], location['lat'],)
