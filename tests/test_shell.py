import unittest
from unittest.mock import Mock, MagicMock

from intelligence.commands import CommandManager
from intelligence.shell import *


class CompleterTestCase(unittest.TestCase):
    def setUp(self):
        self.completer = ConsoleCompleter(['AA', 'AB', 'CD'])

    def test_exist(self):
        res = self.completer.complete('A', 0)
        self.assertEqual(res, 'AA')

    def test_second_attempt(self):
        self.completer.complete('A', 0)
        res = self.completer.complete('A', 1)
        self.assertEqual(res, 'AB')

    def test_different_cases(self):
        res = self.completer.complete('cd', 0)
        self.assertEqual(res, 'CD')

    def test_out_of_cases(self):
        self.completer.complete('C', 0)
        res = self.completer.complete('C', 1)
        self.assertIsNone(res)


class IntelligenceConsoleTestCase(unittest.TestCase):
    def setUp(self):
        self.cmd_manager = CommandManager([
            ('a', lambda a: a, ),
        ])
        self.file = Mock()
        self.file.write = MagicMock()
        self.console = IntelligenceConsole(self.cmd_manager, file=self.file)

    def test_incorrect_manager(self):
        with self.assertRaises(ConsoleError):
            IntelligenceConsole(123)

    def test_parse_line_one_arg(self):
        self.assertEqual(self.console.parse_line('a b'), ('a', ('b',)))

    def test_parse_two_arguments(self):
        self.assertEqual(self.console.parse_line('a b c'), ('a', ('b', 'c',)))

    def test_ignore_spaces(self):
        self.assertEqual(self.console.parse_line('a  b'), ('a', ('b',)))

    def test_too_few_arguments(self):
        with self.assertRaises(ConsoleSyntaxError):
            self.console.parse_line('a')

    def test_empty_command(self):
        self.assertIsNone(self.console.parse_line(''))

    def test_leading_space(self):
        self.assertEqual(self.console.parse_line(' a b'), ('a', ('b',)))

    def test_parse_none(self):
        self.assertIsNone(self.console.parse_line(None))

    def test_run(self):
        self.console.run('a b')
        self.file.write.assert_called_once_with('b\n')

    def test_run_without_args(self):
        self.console.run('a')
        self.file.write.assert_called_once_with('Command syntax error: Command must have at least one argument\n')

    def test_run_not_existent_command(self):
        self.console.run('b c')
        self.file.write.assert_called_once_with('Command \'b\' not found\n')

    def test_run_empty_command(self):
        self.console.run('')
        self.file.write.assert_called_once_with('\n')


if __name__ == '__main__':
    unittest.main()
