import unittest
from intelligence.api import Api, ApiErrorResponse, ApiCityNotFound, ApiErrorResponseWithSuggestions, ApiHelperResponse
from intelligence.db import DbNotFoundError, DbError
from unittest.mock import Mock, MagicMock, ANY


class ApiTestCase(unittest.TestCase):
    def setUp(self):
        self.db_conn = Mock()
        # noinspection PyTypeChecker
        self.api = Api(self.db_conn, Mock())

        self.api._find_city = MagicMock(side_effect=lambda a: (a, (0, 0,),))

    def test_add_agent(self):
        self.db_conn.add_agent = MagicMock()
        resp = self.api.add('Петров', 'Челябинск')

        self.db_conn.add_agent.assert_called_once_with('Петров', 'Челябинск', ANY)
        self.assertEqual(str(resp), 'OK')

    def test_add_wrong_city_name(self):
        def side_effect(city):
            raise ApiCityNotFound('\'{}\' not found'.format(city))

        self.api._find_city = MagicMock(side_effect=side_effect)
        resp = self.api.add('Рабинович', 'Новосйойрск')

        self.assertEqual(type(resp), ApiErrorResponse)
        self.assertFalse(resp.is_successful)
        self.assertEqual(resp.error_msg, '\'Новосйойрск\' not found')

    def test_where_agent(self):
        self.db_conn.get_by_name = MagicMock(return_value={'name': 'Петров', 'city': 'Челябинск'})

        result = self.api.where('Петров')
        self.assertTrue(result.is_successful)
        self.assertEqual(result.city, 'Челябинск')

    def test_where_non_existent_agent(self):
        self.db_conn.get_by_name = MagicMock(return_value=None)

        resp = self.api.where('Петров')
        self.assertEqual(type(resp), ApiErrorResponseWithSuggestions)
        self.assertFalse(resp.is_successful)
        self.assertEqual(resp.error_msg, '\'Петров\' not found')

    def test_where_non_existent_with_suggestions(self):
        self.db_conn.get_by_name = MagicMock(return_value=None)
        self.db_conn.get_similar_names = MagicMock(return_value=['Сидоров'])

        resp = self.api.where('Петров')
        self.assertEqual(type(resp), ApiErrorResponseWithSuggestions)
        self.assertFalse(resp.is_successful)
        self.assertEqual(str(resp), '\'Петров\' not found\nSuggestions: \'Сидоров\'')
        self.assertListEqual(resp.suggestions, ['Сидоров'])

    def test_find_helper(self):
        self.db_conn.get_nearest = MagicMock(return_value=([{'name': 'Сидоров', 'city': 'Москва'}], 725.6483))

        resp = self.api.help('Петров')

        self.assertEqual(type(resp), ApiHelperResponse)
        self.assertTrue(resp.is_successful)
        self.assertListEqual(resp.names, ['Сидоров', ])
        self.assertEqual(resp.distance, 725.6483)
        self.assertEqual(str(resp), 'Сидоров (725.6 km)')

    def test_find_two_helpers(self):
        self.db_conn.get_nearest = MagicMock(
            return_value=([{'name': 'Сидоров', 'city': 'Москва'},
                           {'name': 'Иванов', 'city': 'Москва'}], 725.6483))

        resp = self.api.help('Петров')

        self.assertEqual(type(resp), ApiHelperResponse)
        self.assertTrue(resp.is_successful)
        self.assertListEqual(resp.names, ['Сидоров', 'Иванов'])
        self.assertEqual(resp.distance, 725.6483)
        self.assertEqual(str(resp), 'Сидоров (725.6 km)\nИванов (725.6 km)')

    def test_find_helper_name_not_found(self):
        self.db_conn.get_nearest = MagicMock(side_effect=DbNotFoundError('\'Петров\' not found'))
        self.db_conn.get_similar_names = MagicMock(return_value=['Сидоров'])

        resp = self.api.help('Петров')
        self.assertEqual(type(resp), ApiErrorResponseWithSuggestions)
        self.assertFalse(resp.is_successful)
        self.assertEqual(str(resp), '\'Петров\' not found\nSuggestions: \'Сидоров\'')
        self.assertListEqual(resp.suggestions, ['Сидоров'])

    def test_find_helper_error(self):
        self.db_conn.get_nearest = MagicMock(side_effect=DbError('Something wrong'))

        resp = self.api.help('Петров')
        self.assertEqual(type(resp), ApiErrorResponse)
        self.assertFalse(resp.is_successful)
        self.assertEqual(resp.error_msg, 'Something wrong')


if __name__ == '__main__':
    unittest.main()
