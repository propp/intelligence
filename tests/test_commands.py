import unittest
from intelligence.commands import CommandManager, CommandError, CommandNotFound


class CommandsTestCase(unittest.TestCase):
    def test_empty_manager(self):
        cmd_manager = CommandManager()
        self.assertEqual(len(cmd_manager.get_commands()), 0)

    def test_get_added_command_name(self):
        cmd_manager = CommandManager([
            ('name', lambda a: a, ),
        ])

        self.assertEqual(cmd_manager.get_commands(), ['name'])

    def test_init_with_not_iterable(self):
        with self.assertRaises(CommandError):
            CommandManager('str')

    def test_trying_to_add_non_callable(self):
        with self.assertRaises(CommandError):
            CommandManager((
                ('name', 'not_func',),
            ))

    def test_register_function_after_create(self):
        cmd_manager = CommandManager([
            ('name', lambda a: a, ),
        ])
        cmd_manager.register_command('name2', lambda a: a*2)

        self.assertEqual(len(cmd_manager.get_commands()), 2)

    def test_get_function(self):
        cmd_manager = CommandManager([
            ('name', lambda a: a, ),
        ])

        func = cmd_manager.get_function('name')
        self.assertTrue(callable(func))
        self.assertEqual(func(1), 1)

    def test_get_function_for_unregistered_name(self):
        cmd_manager = CommandManager([
            ('name', lambda a: a, ),
        ])

        with self.assertRaises(CommandNotFound):
            cmd_manager.get_function('name2')

if __name__ == '__main__':
    unittest.main()
